package uk.ac.napier.runningapp;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button tomap_change = (Button) findViewById(R.id.tomap_change); //Code that changes the Activity to the map activity
        tomap_change.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent mapscreen = new Intent(MainActivity.this, MapScreenMain.class);
                startActivity(mapscreen);
            }
        });
        Button timer_change = (Button) findViewById(R.id.timer_change); //Code that changes the Activity to the timer activity
        timer_change.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent timerscreen = new Intent(MainActivity.this, TimerCount.class);
                startActivity(timerscreen);
            }
        });
        Button free_change = (Button) findViewById(R.id.free_change); //Code that changes the Activity to the Free run activity
        free_change.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent freescreen = new Intent(MainActivity.this, FreeRunActivity.class);
                startActivity(freescreen);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
