package uk.ac.napier.runningapp;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

public class FreeRunActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    public int x = 0;
    public Polyline runroute;
    public LatLng oldlatlng;
    public LatLng newlatlng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_screen_main);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        final LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        final MarkerOptions markerOptions = new MarkerOptions();
        final TextView distance_actual = (TextView) findViewById(R.id.distance_actual);
        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                if (x == 0){
                    oldlatlng = new LatLng(location.getLatitude(), location.getLongitude());
                    newlatlng = new LatLng(location.getLatitude(), location.getLongitude());
                    runroute = mMap.addPolyline(new PolylineOptions().geodesic(true)
                                    .add(oldlatlng)
                                    .add(newlatlng)
                                    .width(5)
                    );
                    x++;
                    float[] results = new float[2];
                    Location.distanceBetween(oldlatlng.latitude, oldlatlng.longitude, newlatlng.latitude, newlatlng.longitude, results);
                    double distance = Math.round(results[0]);
                    distance_actual.setText(String.valueOf(distance));
                }
                else{
                    newlatlng = new LatLng(location.getLatitude(), location.getLongitude());
                    runroute = mMap.addPolyline(new PolylineOptions().geodesic(true)
                            .add(oldlatlng)
                            .add(newlatlng)
                            .width(5)
                    );
                    float[] results = new float[2];
                    Location.distanceBetween(oldlatlng.latitude, oldlatlng.longitude, newlatlng.latitude, newlatlng.longitude, results);
                    double distance = Math.round(results[0]);
                    double old = Double.parseDouble((distance_actual.getText()).toString());
                    distance_actual.setText(String.valueOf(distance + old));
                    oldlatlng = newlatlng;

                }
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(newlatlng, 15);
                mMap.animateCamera(cameraUpdate);
            }

            public void onStatusChanged(String provider, int Status, Bundle extras) {}
            public void onProviderEnabled(String provider) {}
            public void onProviderDisabled(String provider) {}
        };

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener(){

            @Override
            public void onMapClick(LatLng latlng) {
            }
        });

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
    }
}