package uk.ac.napier.runningapp;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

public class MapScreenMain extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    public int x = 0;
    public Marker firstmark;
    public Marker secondmark;
    public Polyline runroute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_screen_main);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        final Button begin_run = (Button) findViewById(R.id.begin_run);
        final TextView distance_actual = (TextView) findViewById(R.id.distance_actual);
        begin_run.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent startscreen = new Intent(MapScreenMain.this, DistanceRunStart.class);
                startscreen.putExtra("distance", distance_actual.getText());
                startActivity(startscreen);
            }
        });
        begin_run.setEnabled(false);
        final LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        final MarkerOptions markerOptions = new MarkerOptions();
        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                LatLng latlng = new LatLng(location.getLatitude(), location.getLongitude());
                firstmark = mMap.addMarker(new MarkerOptions()
                        .position(latlng)
                        .draggable(true)
                        .title("Run Start"));
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latlng, 15);
                mMap.animateCamera(cameraUpdate);
                locationManager.removeUpdates(this);
            }

            public void onStatusChanged(String provider, int Status, Bundle extras) {}

            public void onProviderEnabled(String provider) {}
            public void onProviderDisabled(String provider) {}
        };

        googleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {

            @Override
            public void onMarkerDragStart(Marker firstmark) {}

            @Override
            public void onMarkerDrag(Marker firstmark) {}

            @Override
            public void onMarkerDragEnd(Marker firstmark) {
                runroute.remove();
                runroute = mMap.addPolyline(new PolylineOptions().geodesic(true)
                        .add(new LatLng(firstmark.getPosition().latitude, firstmark.getPosition().longitude))//User's first location
                        .add(new LatLng(secondmark.getPosition().latitude, secondmark.getPosition().longitude))
                );//User's selected location
                calculateDistance(firstmark, secondmark, distance_actual);
            }
        });

        googleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {

            @Override
            public void onMarkerDragStart(Marker secondmark) {}

            @Override
            public void onMarkerDrag(Marker secondmark) {}

            @Override
            public void onMarkerDragEnd(Marker secondmark) {
                runroute.remove();
                runroute = mMap.addPolyline(new PolylineOptions().geodesic(true)
                                .add(new LatLng(firstmark.getPosition().latitude, firstmark.getPosition().longitude))//User's first location
                                .add(new LatLng(secondmark.getPosition().latitude, secondmark.getPosition().longitude))
                );//User's selected location
                calculateDistance(firstmark, secondmark, distance_actual);
            }
        });

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng latlng) {
                if (x == 0) {
                    markerOptions.position(latlng);
                    markerOptions.draggable(true);
                    markerOptions.title("Run End");
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(latlng));
                    secondmark = mMap.addMarker(markerOptions);
                    runroute = mMap.addPolyline(new PolylineOptions().geodesic(true)
                            .add(new LatLng(firstmark.getPosition().latitude, firstmark.getPosition().longitude))//User's first location - hopefully the user's location, because I can't figure out a solution
                            .add(new LatLng(secondmark.getPosition().latitude, secondmark.getPosition().longitude)) //User's selected location
                    );
                    x++;
                    begin_run.setEnabled(true);
                    calculateDistance(firstmark, secondmark, distance_actual);
                }
            }
        });

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
    }

    public void calculateDistance(Marker firstmark, Marker secondmark, TextView distance_actual){
        float[] results = new float[2];
        Location.distanceBetween(firstmark.getPosition().latitude, firstmark.getPosition().longitude, secondmark.getPosition().latitude, secondmark.getPosition().longitude, results);
        String distance = String.format("%.2f",results[0]);
        distance_actual.setText(distance);
    }
}