package uk.ac.napier.runningapp;

import android.os.SystemClock;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;
import android.widget.Toast;


public class TimerCount extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer_count);
        final Chronometer runtime = (Chronometer) findViewById(R.id.runtime);
        runtime.setTextSize(40);
        runtime.setBase(SystemClock.elapsedRealtime());
        Button start_button = (Button) findViewById(R.id.start_button);
        Button stop_button = (Button) findViewById(R.id.stop_button);

        start_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runtime.setBase(SystemClock.elapsedRealtime());
                runtime.start();
                TextView calories_burned = (TextView) findViewById(R.id.calories_burned);
                calories_burned.setText("Running...");
            }
        });

        stop_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runtime.stop();
                //0.21 calories per second
                String timespent = (runtime.getText()).toString();
                String[] timeparts = timespent.split(":");
                int minutes = (Integer.parseInt(timeparts[0])*60);
                int seconds = Integer.parseInt(timeparts[1]);
                double burned = ((minutes*0.21)+(seconds*0.21));
                TextView calories_burned = (TextView) findViewById(R.id.calories_burned);
                calories_burned.setText(String.valueOf(Math.round(burned)));
            }
        });

        runtime.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                String timespent = (runtime.getText()).toString();
                String[] timeparts = timespent.split(":");
                int minutes = (Integer.parseInt(timeparts[0])*60);
                int seconds = Integer.parseInt(timeparts[1]);
                double burned = ((minutes*0.21)+(seconds*0.21));
                TextView calories_burned = (TextView) findViewById(R.id.calories_burned);
                calories_burned.setText(String.valueOf(Math.round(burned)));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_timer_count, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
